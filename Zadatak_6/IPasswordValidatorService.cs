﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_6
{
    interface IPasswordValidatorService
    {
        bool IsValidPassword(String candidate);
    }
}
