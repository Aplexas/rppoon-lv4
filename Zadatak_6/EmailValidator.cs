﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_6
{
    class EmailValidator:IEmailValidatorService
    {

        public bool IsValidAddress(String candidate)
        {
            return candidate.Contains("@") && (candidate.Contains(".com") || candidate.Contains(".hr"));
        }

    }
}
