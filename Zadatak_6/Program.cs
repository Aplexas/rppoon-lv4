﻿using System;

namespace Zadatak_6
{
    class Program
    {
        static void Main(string[] args)
        {
            String password = "12345678";
            String email = "test@hotmail.com";
            
            EmailValidator emailValidator = new EmailValidator();
            PasswordValidator passwordValidator = new PasswordValidator(5);
            Console.WriteLine(emailValidator.IsValidAddress(email));
            Console.WriteLine(passwordValidator.IsValidPassword(password));
        }
    }
}
