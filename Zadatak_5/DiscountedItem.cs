﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_5
{
    class DiscountedItem:RentableDecorator
    {
        private readonly double discountInPercentage = 50.00;
        public DiscountedItem(IRentable rentable,double discountInPercentage) : base(rentable) {
            this.discountInPercentage = discountInPercentage;
        }
        public override double CalculatePrice()
        {
            return base.CalculatePrice() + ((this.discountInPercentage*base.CalculatePrice())/100);
        }
        public override String Description
        {
            get
            {
                return  base.Description+ " now at "+this.discountInPercentage+ "% off ";
            }
        }
    }
}
