﻿using System;
using System.Collections.Generic;

namespace Zadatak_5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> bookAndVideo = new List<IRentable>();
            Book book = new Book("The Da Vinci Code");
            Video video = new Video("The Room");
            RentingConsolePrinter printer = new RentingConsolePrinter();
            Book newBook = new Book("Inferno");
            Video newVideo = new Video("Parasyte");
            HotItem hotBook = new HotItem(newBook);
            HotItem hotVideo = new HotItem(newVideo);
            bookAndVideo.Add(book);
            bookAndVideo.Add(video);
            bookAndVideo.Add(hotBook);
            bookAndVideo.Add(hotVideo);

            List<IRentable> flashSales= new List<IRentable>();
            foreach(IRentable item in bookAndVideo)
            {
                flashSales.Add(new DiscountedItem(item,70.00));
            }
            printer.DisplayItems(flashSales);
            printer.PrintTotalPrice(flashSales);

        }
    }
}
