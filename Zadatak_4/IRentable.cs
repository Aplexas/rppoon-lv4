﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_4
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();
    }
}
