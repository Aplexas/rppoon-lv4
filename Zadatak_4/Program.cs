﻿using System;
using System.Collections.Generic;

namespace Zadatak_4
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> bookAndVideo = new List<IRentable>();
            Book book = new Book("The Da Vinci Code");
            Video video = new Video("The Room");
            RentingConsolePrinter printer = new RentingConsolePrinter();
            Book newBook = new Book("Inferno");
            Video newVideo = new Video("Parasyte");
            HotItem hotBook = new HotItem(newBook);
            HotItem hotVideo = new HotItem(newVideo);

            bookAndVideo.Add(book);
            bookAndVideo.Add(video);
            bookAndVideo.Add(hotBook);
            bookAndVideo.Add(hotVideo);
            printer.DisplayItems(bookAndVideo);
            printer.PrintTotalPrice(bookAndVideo);
            
            //Vidimo da se cijena hotitema razlikuje za 1.99 od običnih
        }
    }
}
