﻿using System;
using System.Collections.Generic;

namespace Zadatak_3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> bookAndVideo = new List<IRentable>();
            Book book= new Book("The Da Vinci Code");
            Video video = new Video("The Room");
            RentingConsolePrinter printer = new RentingConsolePrinter();
            bookAndVideo.Add(book);
            bookAndVideo.Add(video);
            printer.DisplayItems(bookAndVideo);
            printer.PrintTotalPrice(bookAndVideo);

        }
    }
}
